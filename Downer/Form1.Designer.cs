﻿namespace Downer
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDownMinutes = new System.Windows.Forms.NumericUpDown();
            this.buttonSetTimer = new System.Windows.Forms.Button();
            this.labelTimer = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDownMinutes
            // 
            this.numericUpDownMinutes.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownMinutes.Location = new System.Drawing.Point(12, 12);
            this.numericUpDownMinutes.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownMinutes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMinutes.Name = "numericUpDownMinutes";
            this.numericUpDownMinutes.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownMinutes.TabIndex = 0;
            this.numericUpDownMinutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDownMinutes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMinutes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDownMinutes_KeyPress);
            // 
            // buttonSetTimer
            // 
            this.buttonSetTimer.Location = new System.Drawing.Point(138, 10);
            this.buttonSetTimer.Name = "buttonSetTimer";
            this.buttonSetTimer.Size = new System.Drawing.Size(75, 23);
            this.buttonSetTimer.TabIndex = 1;
            this.buttonSetTimer.Text = "Get down!";
            this.buttonSetTimer.UseVisualStyleBackColor = true;
            this.buttonSetTimer.Click += new System.EventHandler(this.buttonSetTimer_Click);
            // 
            // labelTimer
            // 
            this.labelTimer.Font = new System.Drawing.Font("NI7SEG", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimer.Location = new System.Drawing.Point(1, 0);
            this.labelTimer.Name = "labelTimer";
            this.labelTimer.Size = new System.Drawing.Size(218, 42);
            this.labelTimer.TabIndex = 2;
            this.labelTimer.Text = "99:99:99";
            this.labelTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTimer.Visible = false;
            this.labelTimer.Click += new System.EventHandler(this.labelTimer_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(221, 42);
            this.Controls.Add(this.buttonSetTimer);
            this.Controls.Add(this.numericUpDownMinutes);
            this.Controls.Add(this.labelTimer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Downer";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinutes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDownMinutes;
        private System.Windows.Forms.Button buttonSetTimer;
        private System.Windows.Forms.Label labelTimer;
    }
}


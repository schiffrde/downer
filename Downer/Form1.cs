﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Management;

namespace Downer
{
    public partial class MainWindow : Form
    {
        Timer shutdownTimer;
        int secondsToShutdown;

        public MainWindow()
        {
            InitializeComponent();
            numericUpDownMinutes.Value = 60;
        }

        /// <summary>
        /// Set and start the timer, show the time dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSetTimer_Click(object sender, EventArgs e)
        {
            secondsToShutdown = Convert.ToInt32(numericUpDownMinutes.Value) * 60;
            shutdownTimer = new Timer();
            shutdownTimer.Interval = 1000;
            shutdownTimer.Tick += new EventHandler(shutdownTimer_Tick);
            shutdownTimer.Start();

            numericUpDownMinutes.Visible = false;
            buttonSetTimer.Visible = false;
            labelTimer.Visible = true;
        }

        /// <summary>
        /// Decrease the remaining time every second and
        /// shutdown when remaining time is zero
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shutdownTimer_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("Remaining seconds: " + secondsToShutdown.ToString());
            DisplayRemainingTime();
            secondsToShutdown--;

            if (secondsToShutdown == 0)
            {
                shutdownTimer.Stop();
                Shutdown();
            }
        }

        /// <summary>
        /// Format and display the remaining time
        /// </summary>
        private void DisplayRemainingTime()
        {
            TimeSpan t = TimeSpan.FromSeconds(secondsToShutdown);
            labelTimer.Text = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds);
        }

        /// <summary>
        /// Shutdown the pc
        /// </summary>
        void Shutdown()
        {
            ManagementBaseObject mboShutdown = null;
            ManagementClass mcWin32 = new ManagementClass("Win32_OperatingSystem");
            mcWin32.Get();

            // You can't shutdown without security privileges
            mcWin32.Scope.Options.EnablePrivileges = true;
            ManagementBaseObject mboShutdownParams =
                     mcWin32.GetMethodParameters("Win32Shutdown");

            // Flag 1 means we want to shut down the system. Use "2" to reboot.
            mboShutdownParams["Flags"] = "1";
            mboShutdownParams["Reserved"] = "0";
            foreach (ManagementObject manObj in mcWin32.GetInstances())
            {
                mboShutdown = manObj.InvokeMethod("Win32Shutdown",
                                               mboShutdownParams, null);
            }
        }

        /// <summary>
        /// Cancel shutdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelTimer_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Runterfahren abbrechen?", "Ehrlich?", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                shutdownTimer.Stop();
                labelTimer.Visible = false;
                numericUpDownMinutes.Visible = true;
                buttonSetTimer.Visible = true;
            }
        }

        /// <summary>
        /// listen to "enter" presses on the numeric input field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDownMinutes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                buttonSetTimer_Click(sender, e);
            }
        }
    }
}
